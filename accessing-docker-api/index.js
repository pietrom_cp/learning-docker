const http = require('http');

const options = {
  socketPath: '/var/run/docker.sock',
  path: 'http:/v1.24/images/json'
};

const callback = resp => {
  console.log(`STATUS: ${resp.statusCode}`);

	var body = '';

    resp.on('data', (chunk) => {
	  body += chunk;
    }).on('end', () => {
        var respArr = JSON.parse(body + '');

        respArr.forEach((img) => {
            //console.log(img)
            const tags = img.RepoTags
            if(tags) {
              console.log(`${img.Id} --> ${tags.join(', ')}`)
            } else {
              console.log(img.Id)
            }
            //console.log(value.Spec.Name + ' replicas=' + value.Spec.Mode.Replicated.Replicas);

        })

    }).on('error', data => console.error(data));

}

const clientRequest = http.get(options, callback);
clientRequest.end(0);