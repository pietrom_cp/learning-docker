def define_vm(config, name, baseBox, baseBoxVersion, ip, diskSize = nil, primary = false)
  config.vm.define name, primary: primary, autostart: primary do |node|
    node.vm.box = baseBox
    node.vm.box_version = baseBoxVersion
    unless diskSize.nil?
      node.disksize.size = diskSize
    end
    node.vm.network :private_network, ip: ip, :netmask => "255.255.0.0"
    node.vm.hostname = name
    node.vm.provider "virtualbox" do |vb|
      vb.name = name
      vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
      vb.customize ["modifyvm", :id, "--natdnsproxy1", "on"]
      vb.customize ["modifyvm", :id, "--groups", "/learning-docker"]
      vb.memory = 2048
      vb.cpus = 2
    end
  end
end

def define_docker_vm(config, namePrefix, i, ip)
  define_vm config, "#{namePrefix}#{i}", "pietrom/debian10-docker", "1.3.2", ip
end

Vagrant.configure("2") do |config|
  system "vagrant plugin install vagrant-disksize" unless Vagrant.has_plugin? "vagrant-disksize"

  (0..3).each do |i|
    define_docker_vm config, "bee", i, "172.29.6.#{10 + i}"
  end

  (0..3).each do |i|
    define_vm config, "locust#{i}", "pietrom/debian10", "1.2.2", "172.29.7.#{10 + i}"
  end

  define_vm config, "crabro0", "StefanScherer/windows_2019_docker", "2020.02.24", "172.29.6.20"

  define_vm config, "honeycomb", "pietrom/debian10", "1.2.2", "172.29.6.200"

  define_vm config, "harbor", "pietrom/debian10", "1.2.2", "172.29.17.5", "30GB", primary: true
end