const { BlobServiceClient } = require('@azure/storage-blob')
const uuidv1 = require('uuid/v1')

async function main() {
    console.log('Azure Blob storage v12 - JavaScript quickstart sample')
    const AZURE_STORAGE_CONNECTION_STRING = process.env.AZURE_STORAGE_CONNECTION_STRING
    // Quick start code goes here
    const blobServiceClient = await BlobServiceClient.fromConnectionString(AZURE_STORAGE_CONNECTION_STRING);

	// Create a unique name for the container
	const containerName = 'test-container';

	console.log('\nCreating container...');
	console.log('\t', containerName);

	// Get a reference to a container
	const containerClient = await blobServiceClient.getContainerClient(containerName);

	// Create the container
	const alreadyPresent = await containerClient.exists();
	if(!alreadyPresent) {
		const createContainerResponse = await containerClient.create();
		console.log("Container was created successfully. requestId: ", createContainerResponse.requestId);
	}	

	const blobName = 'quickstart' + uuidv1() + '.txt';

// Get a block blob client
	const blockBlobClient = containerClient.getBlockBlobClient(blobName);

	console.log('\nUploading to Azure storage as blob:\n\t', blobName);

	// Upload data to the blob
	const data = 'Hello, World!';
	const uploadBlobResponse = await blockBlobClient.upload(data, data.length);
	console.log("Blob was uploaded successfully. requestId: ", uploadBlobResponse.requestId);

	console.log('\nListing blobs...');

	// List the blob(s) in the container.
	for await (const blob of containerClient.listBlobsFlat()) {
	    console.log('\t', blob.name);
	}
}

main().then(() => console.log('Done')).catch((ex) => console.log(ex.message))