`gcc -o hello-from-scratch-with-parameters-reloaded/hello --static hello-from-scratch-with-parameters-reloaded/hello.c`

`docker build -t hello-from-scratch-with-parameters-reloaded ./hello-from-scratch-with-parameters-reloaded`

`docker run --rm hello-from-scratch-with-parameters-reloaded`

`docker run --rm hello-from-scratch-with-parameters-reloaded Pietro`

`docker run --rm hello-from-scratch-with-parameters-reloaded /hello Pietro`