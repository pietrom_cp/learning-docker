`gcc -o hello-from-scratch-with-parameters/hello --static hello-from-scratch-with-parameters/hello.c`

`docker build -t hello-from-scratch-with-parameters ./hello-from-scratch-with-parameters`

`docker run --rm hello-from-scratch-with-parameters`

`docker run --rm hello-from-scratch-with-parameters /hello Pietro`