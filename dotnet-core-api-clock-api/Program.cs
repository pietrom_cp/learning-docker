﻿using System;
using System.Linq;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Builder;
using Newtonsoft.Json;
using Microsoft.AspNetCore.Http;
using System.IO;

namespace ClockApi
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var port = Environment.GetEnvironmentVariable("PORT") ?? "80";
            Console.WriteLine($"Running Kestrel on port {port}");

            var host = new WebHostBuilder()
                .UseKestrel()
                .Configure(app => app.Map("/clock", ClockHandler))
                .UseUrls($"http://0.0.0.0:{port}")
                .Build();

            host.Run();
        }

        private static void ClockHandler(IApplicationBuilder app)
        {
            app.Run(async context =>
            {
                context.Response.ContentType = context.Request.ContentType;
                await context.Response.WriteAsync(
                    JsonConvert.SerializeObject(new
                    {
                        Now = DateTimeOffset.Now,
                        UtcNow = DateTimeOffset.UtcNow,
                        TimeZone = TimeZoneInfo.Local.Id
                    })
                );
            });
        }
    }
}