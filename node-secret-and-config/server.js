const fs = require('fs');
const util = require('util');
const express = require('express')
const app = express()
const port = 4000

app.get('/', async (req, res) => {	
	writeParagraph(res, `Hostname: ${process.env.HOSTNAME}`)
	writeParagraph(res, `Config: ${await readConfig()}`)
	writeParagraph(res, `Secret: ${await readSecret()}`)
	writeParagraph(res, `Config (custom location): ${await readCustomConfig()}`)
	writeParagraph(res, `Secret (custom location): ${await readCustomSecret()}`)
	res.status(200)
	res.end()
})

function writeParagraph(res, content) {
	res.write(`<p>${content}</p>`)	
}

const readFile = util.promisify(fs.readFile)

function readConfig() {
	return readFile('/my-config')
}

function readConfigAsEnv() {
	return process.env.MY_CONFIG
}

function readCustomConfig() {
	return readFile('/config/my-config')
}

function readSecret() {
	return readFile('/run/secrets/my-secret')
}

function readCustomSecret() {
	return readFile('/secrets/my-secret')
}

app.listen(port, () => console.log(`Example app listening on port ${port}!`))