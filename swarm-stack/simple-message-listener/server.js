var q = 'TheQueue';
 
function bail(err) {
  console.error('=== ERROR', err);
  process.exit(1);
}

console.log('V2V2V2 Start connections...')
Promise.all([
    require('mongodb').MongoClient.connect('mongodb://mongodb:27017/test1'),
    require('amqplib').connect('amqp://user:pass@rabbitmq/vhost')
]).then(([db, bus]) => {
  console.log('Connected')
  bus.createChannel().then(ch => {
    ch.assertQueue(q).then(() => {
      console.log('Queue', q, 'ok - Register subscriber')
      ch.consume(q, msg => {
        console.log('Received message', msg && msg.content.toString())
        if (msg !== null) {
          db.collection('test').insert({ when: new Date(), what: msg.content.toString()})
          .then(() => {
            ch.ack(msg)
          }).catch(bail)
        }
      })
    })
  }).catch(bail)
}).catch(bail)
